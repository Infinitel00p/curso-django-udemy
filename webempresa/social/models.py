from django.db import models

# Create your models here.
class Link(models.Model):
    key = models.SlugField(max_length = 200, verbose_name='Nombre clave', unique=True)
    name = models.CharField(max_length = 70, verbose_name='Nombre')
    url = models.URLField(max_length = 200, verbose_name='Enlace', null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de actualización')
    


    class Meta:
        ordering = ['name']
        verbose_name = 'enlance'
        verbose_name_plural = 'enlaces'

    def __str__(self):
        return self.name