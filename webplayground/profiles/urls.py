from django.contrib import admin
from django.urls import path, include
from pages.urls import pages_patterns
from django.conf import settings
from .views import ProfileViewList, ProfileDetailView

profiles_patterns = ([
    path('', ProfileViewList.as_view(), name='list'),
    path('', ProfileDetailView.as_view(), name='detail'),
    
], 'profiles')