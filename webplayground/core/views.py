from django.shortcuts import render

from django.views.generic.base import TemplateView

class Home(TemplateView):
    template_name = 'core/home.html'

    """
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["title"] = 'Mi super web'
        return context
    """
    def get(self, request, *args, **kwards):
        return render(request, self.template_name, {'title':'Mi super web'})

class Sample(TemplateView):
    template_name = 'core/sample.html'

