from django.db import models

# Create your models here.
class Project(models.Model):
    title = models.CharField(max_length=70, verbose_name='titulo')
    description = models.TextField(verbose_name='Descripción')
    image = models.ImageField(verbose_name='Imagen', upload_to="projects")
    link = models.URLField(null=True, blank=True, verbose_name='Dirección web')
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de creación')

    class Meta:
        verbose_name = 'proyecto'
        verbose_name_plural = 'Proyectos'
        ordering = ["-created"]

    def __str__(self):
        return self.title